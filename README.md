# OracleLinux/RedHat Universal Forwarder Removal

This Role will remove the [Splunk Universal Forwarder](https://gitlab.com/planforfailure/ansible-universal-forwarder) on OracleLinux/RedHat hosts. A broader overview of Universal Forwarders in other Linux distributions can be found below.

- https://docs.splunk.com/Splexicon:Universalforwarder
- https://docs.splunk.com/Documentation/Splunk/8.0.4/Installation/InstallonLinux
- https://docs.splunk.com/Documentation/InfraApp/2.1.0/Admin/collectdSourceCommands
- https://docs.splunk.com/Documentation/InfraApp/2.1.0/Admin/ManageAgents


## Requirements
This role works on OracleLinux/RedHat derivatives

## Role Variables
```
splunk_forwarder_user: splunk
splunk_forwarder_group: splunk
usr_id: ME
action_completed: Universal Forwarder removed
```
## Components
All tasks run [sequentionally](roles/ansible-universal-forwarder-removal/tasks/main.yml)

## Dependencies
N/A

## Summary
To completely remove the Universal Forwarder, run the following

`$ ansible-playbook remove_uf.yml -bK`

## Licence

MIT/BSD
